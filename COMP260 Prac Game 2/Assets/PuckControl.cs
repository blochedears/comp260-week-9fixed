﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour {

	private AudioSource audio;
	public Transform startingPos;
	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
		rigidbody = GetComponent<Rigidbody> ();
		ResetPosition ();
	
	}
	public AudioClip wallCollideClip;
	public AudioClip paddleCollideClip;
	public LayerMask paddleLayer;

	void OnCollisionEnter(Collision collision) {
		if (paddleLayer.Contains (collision.gameObject)) {
			audio.PlayOneShot (wallCollideClip);
		}
		else {
			audio.PlayOneShot (wallCollideClip);
	}
}

	public void ResetPosition() {
		rigidbody.MovePosition (startingPos.position);
		rigidbody.velocity = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
